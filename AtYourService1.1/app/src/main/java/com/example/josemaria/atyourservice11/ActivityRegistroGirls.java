package com.example.josemaria.atyourservice11;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by josemaria on 10/08/2015.
 */
public class ActivityRegistroGirls extends ActionBarActivity {
        private Button Aceptar;

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_register_girls);

            Aceptar = (Button)findViewById(R.id.btnAceptar);

            Aceptar.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    Intent PantallaNueva = new Intent(ActivityRegistroGirls.this,log_in.class);
                    startActivity(PantallaNueva);
                }
            });
        }
    }
